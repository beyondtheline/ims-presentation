# Individual Motion Space
---
# Entstehung

- Entstanden im Kurs Individual Design bei Prof. Klaus Teltenkötter und Prof. Bernd Benninghoff

## Projektidee mit Moritz Frank und Tobias Scheeder

### Wie kann aus Bewegung Raum entstehen?

### Experimentelle Entwicklung

* Bewegung von Personen aufzeichnen und digital zu räumliche Strukturen umwandeln

![Hocke](./assets/PITCHME-68b27038.JPG)

![Hocke2](assets/PITCHME-923f390b.jpg)
